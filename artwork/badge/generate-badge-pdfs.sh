#!/bin/sh

# Prereqs:  xmlstarlet inkscape fonts-freefont-otf fonts-freefont-ttf
#
# Usage:  generate-badge-pdfs.sh nick1 name1 nick2 name2 nick3 name3

TEMPLATE="debconf14-badge-template.svg"

generate_one_pdf() {
    local nick1="$1"
    local name1="$2"
    local nick2="$3"
    local name2="$4"
    local nick3="$5"
    local name3="$6"

    if [ -z "$7" ] ; then
    	fn="$1"
    else
	fn="$7"
    fi

    local rootname="dc14-badges-${fn}"

    # Copy template to new inkscape file.
    local svgname="${rootname}.svg"
    echo "Generating svg in '${svgname}'"
    cp ${TEMPLATE} ${svgname}

    # Modify inkscape file.
    xmlstarlet ed -P -L \
      -u '//_:text[@id="name-top-left"]/_:tspan' -v "${name1}" \
      -u '//_:text[@id="nick-top-left"]/_:tspan' -v "${nick1}" \
      -u '//_:text[@id="name-top-right"]/_:tspan' -v "${name1}" \
      -u '//_:text[@id="nick-top-right"]/_:tspan' -v "${nick1}" \
      -u '//_:text[@id="name-mid-left"]/_:tspan' -v "${name2}" \
      -u '//_:text[@id="nick-mid-left"]/_:tspan' -v "${nick2}" \
      -u '//_:text[@id="name-mid-right"]/_:tspan' -v "${name2}" \
      -u '//_:text[@id="nick-mid-right"]/_:tspan' -v "${nick2}" \
      -u '//_:text[@id="name-bot-left"]/_:tspan' -v "${name3}" \
      -u '//_:text[@id="nick-bot-left"]/_:tspan' -v "${nick3}" \
      -u '//_:text[@id="name-bot-right"]/_:tspan' -v "${name3}" \
      -u '//_:text[@id="nick-bot-right"]/_:tspan' -v "${nick3}" \
      ${svgname}

    # Generate pdf from inkscape file.
    local pdfname="${rootname}.pdf"
    echo "Generating pdf in '${pdfname}'"
    inkscape --export-area-page --export-pdf="${pdfname}" --export-text-to-path "${svgname}"
}

generate_one_pdf "$@"



