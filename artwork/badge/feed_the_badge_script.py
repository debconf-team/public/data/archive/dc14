#!/usr/bin/python
#
# feed_the_badge_script.py
#
# sample usage:
# 
#   tail -n +2 registered-attendees-20140820.csv | ./feed_the_badge_script.py
#
#################################################################

import fileinput
import logging
import re
import subprocess

csvline = re.compile('\s*"(.+)"\s*,\s*"(.*)"\s*')

def generatePdf(nameargs):
    global fileCount
    argList = ['./generate-badge-pdfs.sh']
    fn = '%03d_%s_%s_%s' % (fileCount, nameargs[0], nameargs[2], nameargs[4])
    fn = fn.replace(' ', '')
    fn = fn.lower()
    subprocess.call(argList + nameargs + [fn])
    fileCount += 1
    return

names = []
lineCount = 0
fileCount = 0
for line in fileinput.input():
    match = csvline.match(line)
    if match is None:
        logging.info('skipping line: %s' % (line))
        continue

    lineCount += 1

    names += [match.group(2), match.group(1)]
    logging.info('added participant %d' % (lineCount))
    #print 'names = %s' % names

    if lineCount % 3 == 0:
        logging.info('generating %s' % (names))
        generatePdf(names)
        names = []

    # end for each line

# pick up any strays so we pass dummy args to the script
if (lineCount % 3) != 0:
    for x in range (lineCount % 3, 3):
        names += [" ", " " ]
    generatePdf(names)
