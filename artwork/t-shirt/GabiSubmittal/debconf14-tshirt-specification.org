* Overview
  Three different t-shirts are to be printed:  Attendee, Staff, Video.
  All three use the same screens and inks on the frontside, but some
  different screens and/or inks on the backside.

  Frontside has 4 spot colors (roughly white, green, red, black) with
  non-overlapping halftones.  Backside has 1 spot color (white or black
  depending on the shirt).

* Shirt Selection
  Shirts will be Yazbek style C0300 (men's) or D0300 (women's).
** Colors
   - Attendee:  Turquoise (Turquesa)
   - Staff:     Charcoal (Carbón)
   - Video:     Bright Yellow (Canario) 
** Sizes
   TO BE DETERMINED

* Front Printing
  The frontside is 4 inks printed on a colored shirt to effect a visual
  result of ~6 different colors.  Each ink is printed with halftones,
  but the halftones do not overlap.

  The same screens and inks are to be used on all three t-shirt varieties.

** Inks
   - WHITE:  white
   - GREEN:  a green which approximates sRGB #0fda2a
   - RED:    "Debian Red", a red which approximates sRGB #d70751
   - BLACK:  black

** Layers
*** WHITE
    - solid for:
        - Mt Hood (mountain)
        - underbase/shadow for "DebConf"
        - underbase for the rose (no spread)
    - halftone to produce sky/clouds
*** GREEN
    - solid for rose stem/leaves (over white underbase)
    - solid/halftone for green field and transition to background blue
    - specifically no white underbase underneath the green 'field',
      with the intent that it ends up with slightly different tone/texture
      from the rose stem/petals
*** RED
    - solid for debian swirl (over the white underbase)
    - halftone for petals (over the white underbase)
*** BLACK
    - solid text
    - stroke on rose stem/leaves, with centerline trapping
    - halftone for shadows on Mt Hood

* Back Printing
  The backside is a single ink, white or black depending on the color of
  the underlying T-shirt.  All three shirts have the "sponsors" layout,
  but the Staff and Video shirts have additional text above.
** Attendee shirt
   - white ink on turquoise shirt
   - no additonal text above sponsor logos
   - debconf-tshirt-back-attendee.svg
** Staff shirt
   - white ink on charcoal shirt
   - "STAFF" above sponsor logos
   - debconf-tshirt-back-staff-video.svg with "STAFF" layer
** Video shirt
   - black ink on yellow shirt
   - "VIDEO" above sponsor logos
   - debconf-tshirt-back-staff-video.svg with "VIDEO" layer

