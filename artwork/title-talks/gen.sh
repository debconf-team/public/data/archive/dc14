#!/bin/bash

#   gen.sh - Script generator templates PNG talks
#   Copyleft (cc) 2014 Valessio Brito <valessio@debianart.org>
#   This work is licensed as public domain.

# Dependencies:
# apt-get install inkscape xmlstarlet wget

# Get XML
wget "https://summit.debconf.org/debconf14.xml" 2> /dev/null

# Config local files
EVENTDATA=$PWD/debconf14.xml
TEMPLATE=$PWD/template.svg
PNGOUTPUT=$PWD/files
mkdir -p $PNGOUTPUT
SVGOUTPUT=$( mktemp )

# ID Event
idEvent=`xmlstarlet sel -t -v "//schedule/day/room/event/@id" $EVENTDATA`

for itemEvent in $idEvent ; do
    # Cache VAR elements
    EventTrack=$(xmlstarlet sel -t -v "//schedule/day/room/event[@id=$itemEvent]/track" $EVENTDATA|head -1)
    EventTitle=$(xmlstarlet sel -t -v "//schedule/day/room/event[@id=$itemEvent]/title" $EVENTDATA|head -1|sed s/#/*/g)
    EventPerson=$(xmlstarlet sel -t -v "//schedule/day/room/event[@id=$itemEvent]/persons/person" $EVENTDATA|uniq | sed ':a;N;$!ba;s/\n/, /g')
    # Copy template
    cp $TEMPLATE $SVGOUTPUT
    # Change VAR in Template
    sed -i -e "s#TRACK#$(echo $EventTrack)#g" $SVGOUTPUT
    sed -i -e "s#TITLE#$(echo $EventTitle)#g" $SVGOUTPUT
    sed -i -e "s#PERSON#$(echo $EventPerson)#g" $SVGOUTPUT
    # Generator PNG
    inkscape -z $SVGOUTPUT -e $PNGOUTPUT/$itemEvent.png 2> /dev/null
done
