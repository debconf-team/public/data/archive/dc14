Some guidance on using the site templates:

* You need the libtemplate-perl and libtemplate-plugin-xml-perl packages.
* page.tt is the general template (headers, sidebars, footers).
* Check and adjust directories in the ../bin/update-website script to test it on your local computer.
